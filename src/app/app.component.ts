import { Component, OnInit } from '@angular/core';
import { StationService } from './services/station.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor( public service: StationService) {

  }

  longitude = 12.354722;
  latitude = 7.369722;


  
  ngOnInit() {
    this.service.getStations();
  }


  

}