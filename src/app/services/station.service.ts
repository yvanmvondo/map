import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { OMEGA } from 'src/assets/mock-data';
import { Station } from '../models/stations.model';


@Injectable({
  providedIn: 'root'
})
export class StationService {
  private _stations$ = new BehaviorSubject<Station[]>([]);
  stations$ = this._stations$.asObservable();
  currentStation$ = new BehaviorSubject<Station | null>(null);

  getStations(): void {
    this._stations$.next(OMEGA)
  }


  setSelectedStationHandler(station: Station | null) {
    this.currentStation$.next(station);
  }


}
