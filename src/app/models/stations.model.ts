
export interface Station {
        id: number;
        name: string;
        statut: string;
        coords: Coords;
        availableLiquids: Liquids[];
}

export interface Liquids {
        designation: string;
        volume: number;
}

export type Coords = [ number, number];
