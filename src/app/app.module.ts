import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { StationService } from './services/station.service';
import { StationListComponent } from './components/station-list/station-list.component';
import { StationMapComponent } from './components/station-map/station-map.component';
import { StationModalComponent } from './components/station-modal/station-modal.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MapComponent } from './components/map/map.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { SaveComponent } from './modals/save/save.component';
import { ParametresComponent } from './modals/parametres/parametres.component';
import { DetailsComponent } from './modals/details/details.component';
import { SearchPipe } from './pipes/search.pipe';

@NgModule({
  declarations: [
    AppComponent,
    StationListComponent,
    StationMapComponent,
    StationModalComponent,
    MapComponent,
    DashboardComponent,
    SaveComponent,
    ParametresComponent,
    DetailsComponent,
    SearchPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule
  ],
  providers: [StationService],
  bootstrap: [AppComponent],
})
export class AppModule { }
