import {
  Component,
  ElementRef,
  Input,
  OnChanges,
  EventEmitter,
  Output,
  SimpleChanges,
  ViewChild,
  ChangeDetectionStrategy
} from '@angular/core';
import * as L from 'leaflet';
import { Coords, Station } from 'src/app/models/stations.model';

/*   const mapTheme = 'https://tiles.stadiamaps.com/tiles/alidade_smooth_dark'; */
 const mapTheme = 'https://{s}.tile.openstreetmap.org';
/*  const mapTheme = 'http://maps.wikimedia.org/osm-intl' ; */

const IconWhite = L.icon({
  iconUrl: './assets/marker_white.png',
  iconSize: [34, 44],
  iconAnchor: [17, 42],
});

const IconRed = L.icon({
  iconUrl: './assets/marker_red.png',
  iconSize: [34, 44],
  iconAnchor: [17, 42],
});


@Component({
  selector: 'app-station-map',
  templateUrl: './station-map.component.html',
  styleUrls: ['./station-map.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StationMapComponent implements OnChanges  {
  leafletMap!: L.Map;
  @ViewChild('host', {static: true}) host!: ElementRef<HTMLDivElement>;
  @Input() stations: Station[] | null = null;
  @Input() currentStation: Coords | undefined;
  @Output() markerClick = new EventEmitter<Station>();

  constructor() { 
    
  }

  ngOnChanges(changes: SimpleChanges) {
     console.log(this.host.nativeElement)

    if ( changes.stations && changes.stations.isFirstChange()) {
      /* console.log('initialisation') */
      this.initMap();
    }

    if ( changes.currentStation.currentValue) {
      /* console.log(changes.currentStation) */
      this.leafletMap.setView(changes.currentStation.currentValue, 50);
    } else {
      /* console.log('Montrer Toutes les stations') */
      this.fitBounds();
    }
  }

  initMap() { 
    this.leafletMap = L.map(this.host.nativeElement);
    this.leafletMap.attributionControl.setPrefix('Crée par Yvan Mvondo');
    const mainLayer = L.tileLayer(mapTheme + '/{z}/{x}/{y}.png');
    mainLayer.addTo(this.leafletMap);
    this.dessinerMarkers();
  }

  fitBounds() {
    const coords = this.stations?.map( m => m.coords)
    if (coords) {
      this.leafletMap.fitBounds(coords, {
        padding: [10, 10],
      });
    }
  }

  dessinerMarkers(): void {
    this.stations?.forEach( station => {
      const sieges = station.availableLiquids
      new L.Marker(station.coords, { icon: sieges.length ? IconWhite : IconRed })
        .bindTooltip(
          `<strong class="text-danger text-center">${station.name}</strong>:` + '<br>' +
          `<p class="text-center">${sieges.length ? sieges.length + ' capteurs disponibles' : ' aucun capteur disponible'}</p>`
        )
        .on('click', () => {
          if(sieges.length) {
            this.markerClick.emit(station);
          }
        })
        .addTo(this.leafletMap);
    })
  }



}
