import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Station } from 'src/app/models/stations.model';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-station-list',
  templateUrl: './station-list.component.html',
  styleUrls: ['./station-list.component.scss']
})
export class StationListComponent implements OnInit {

  @Input() items: Station[] | null = [];
  @Output() itemClick = new EventEmitter<Station | null>();

  input: FormControl = new FormControl();
  search$ = this.input.valueChanges.pipe(debounceTime(1000));

  constructor() { }

  ngOnInit(): void {
  }

}