import { Pipe, PipeTransform } from '@angular/core';
import { Station } from '../models/stations.model';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(items: Station[] | null, text: string): Station[] | null {
    /* console.log(items, text) */
    if ( !text || !items ) {
      return items;
    }
    return items.filter(item => {
      const findIndex = item.name.toLowerCase().indexOf(text.toLowerCase());
      return findIndex >= 0;
    });
  }

}
