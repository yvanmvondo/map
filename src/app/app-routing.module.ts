import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
/*   {
    path: 'dashboard',
    loadChildren: () =>
      import('./dashboard/dashboard.module').then(
        m => m.DashboardModule
      )
  }, */

/*   {
    path: 'list',
    component: StationListComponent
  },
    {
    path: 'dash',
    component: DashboardComponent
  },
  {
    path: '',
    redirectTo: 'dash',
    pathMatch: 'dash'
  },
  {
    path: '**',
    redirectTo: 'dash',
  } */

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
