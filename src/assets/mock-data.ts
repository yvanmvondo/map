import { Station } from "src/app/models/stations.model";

export const OMEGA: Station[] = [
    {
        id: 1,
        name: 'Tradex Bastos',
        statut: 'En Activité',
        coords: [3.883, 11.5127],
        availableLiquids: []
    },
    {
        id: 2,
        name: 'Tradex Emana',
        statut: 'En Activité',
        coords: [3.9264, 11.5202],
        availableLiquids: [
            {
                designation: 'Super Maxiboost',
                volume: 2222
            },
            {
                designation: 'Petrole',
                volume: 2222
            }
        ]
    },
    {
        id: 3,
        name: 'Tradex Eleveur',
        statut: 'En Activité',
        coords: [3.9011, 11.5584],
        availableLiquids: [
            {
                designation: 'Gasoil Classic',
                volume: 2222
            },
            {
                designation: 'Super Maxiboost',
                volume: 2222
            },
            {
                designation: 'Petrole',
                volume: 2222
            }
        ]
    },
    {
        id: 4,
        name: 'Tradex Simbock',
        statut: 'En Activité',
        coords: [ 3.8264988, 11.4709307 ],
        availableLiquids: []
    },
    {
        id: 5,
        name: 'Tradex Nkoabang',
        statut: 'En Activité',
        coords: [ 3.859962, 11.591562 ],
        availableLiquids: [
            {
                designation: 'Gasoil Classic',
                volume: 2222
            },
            {
                designation: 'Super Classic',
                volume: 2222
            },
            {
                designation: 'Super Maxiboost',
                volume: 2222
            },
            {
                designation: 'Petrole',
                volume: 2222
            }
        ]
    }
]